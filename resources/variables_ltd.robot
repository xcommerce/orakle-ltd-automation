*** Variables ***
${BROWSER}            ${ROBOT_BROWSER}

&{SERVER}              live=https://webshopgb.orakel.com/en

${VALID_USER}          alexandra@xcommerce.eu
${VALID_PASSWORD}      123456

${HOMEPAGE_URL}         ${SERVER.${ENVIRONMENT}}

${SHOP_BROWSE}          ${SERVER.${ENVIRONMENT}}/eu/en/2-home

${ACCOUNT_ORDERS}       ${SERVER.${ENVIRONMENT}}/eu/en/my-account

${PAYPLAZA_PAGE}        ${SERVER.${ENVIRONMENT}}/nl/module/payplaza/validation?brand=visa&sel=Payplaza


#  USER _________________________________________________________
${DELAY}                0
#Add to cart
${successfully_add_to_cart_popup}   css=div[id="popup_confirm"]

#Login Popup
${login_popup}                      css=div[id="login_popup"]

#Remove Cart
${remove_1}                         xpath=//*[@id="tab_shopping_cart"]/div[3]/div/div/div[1]/div[1]/div/div[2]/div[2]/a
${remove_2}                         xpath=//*[@id="tab_shopping_cart"]/div[4]/div/div/div[1]/div[1]/div/div[2]/div[2]/a
${remove_3}                         xpath=//*[@id="tab_shopping_cart"]/div[5]/div/div/div[1]/div[1]/div/div[2]/div[2]/a
${remove_4}                         xpath=//*[@id="tab_shopping_cart"]/div[6]/div/div/div[1]/div[1]/div/div[2]/div[2]/a

#Overview and remarks
${submit_terms}                     css=input[id="terms"]

#Details Page
${DETAILS_PAGE}                     css=#center_column > div:nth-child(2) > div.primary_block.row
${add_to_cart_details_page}         css=a[class="add_to_cart_btn"]
${button_add_to_cart_homepage}      css=a[id="btn_860"]
${popup_go_to_cart}                 xpath=//*[@id="tab_cart_box"]/div
${button_go_to_cart}                xpath=//*[@id="tab_cart_box"]/div/div[2]/div[2]/a

${get_price0}                       €0.00

# Header
${login_buuton}                     xpath=//*[@id="tab_login_links"]
${search_field}                     css=input[id="prodsearch"]
${search_button}                    xpath=//*[@id="searchBlock"]/form/button
${search_autocomplete}              xpath=//*[@id="as_ul"]/li[2]


